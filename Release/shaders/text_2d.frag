#version 330 core
in vec2 TexCoords;
in vec3 TexColor;
out vec4 color;

uniform sampler2D text;
uniform vec3 textColor;
uniform bool useGradient;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
    if(useGradient)
        color = vec4(TexColor, 1.0) * sampled;
    else
        color = vec4(textColor, 1.0) * sampled;
}  