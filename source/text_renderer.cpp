#include "text_renderer.h"

TextRenderer::TextRenderer(GLuint width, GLuint height)
{
    // 加载并配置着色器
    this->TextShader = ResourceManager::LoadShader("shaders/text_2d.vert", "shaders/text_2d.frag", nullptr, "text");
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.0f);
    this->TextShader.SetMat4("projection", projection, GL_TRUE);
    this->TextShader.SetInt("text", 0);
    // 为纹理四边形配置VAO/VBO
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    glBindVertexArray(this->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 7, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(4 * sizeof(GLfloat)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void TextRenderer::Load(std::string font, GLuint fontSize)
{
    // 清除之前加载的字符
    this->Characters.clear();
    // 初始化并加载FreeType库
    FT_Library ft;
    if (FT_Init_FreeType(&ft)) // 每当发生错误时，所有函数都返回一个不同于0的值
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
    // 将字体加载为face
    FT_Face face;
    if (FT_New_Face(ft, font.c_str(), 0, &face))
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
    // 设置大小并加载图示符为
    FT_Set_Pixel_Sizes(face, 0, fontSize);
    // 禁用字节对齐限制
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    // 对于前128个ASCII字符，预加载/编译它们的字符并存储它们
    for (GLubyte c = 0; c < 128; c++)
    {
        // 加载字符图示符
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }
        // 生成纹理
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );
        // 设置纹理选项
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // 存储字符以备将来使用
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            face->glyph->advance.x
        };
        Characters.insert(std::pair<GLchar, Character>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    // 一旦我们完成，就释放FreeType
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

void TextRenderer::RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
{
    // 激活相应的渲染状态
    this->TextShader.Use();
    this->TextShader.SetInt("useGradient", 0);
    this->TextShader.SetVec3("textColor", color);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(this->VAO);

    // 遍历所有字符
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = Characters[*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y + (this->Characters['H'].Bearing.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;
        // 更新每个字符的VBO
        GLfloat vertices[6][7] = {
            { xpos,     ypos + h,   0.0, 1.0 ,  1.0,1.0,1.0},
            { xpos + w, ypos,       1.0, 0.0 ,  1.0,1.0,1.0},
            { xpos,     ypos,       0.0, 0.0 ,  1.0,1.0,1.0},

            { xpos,     ypos + h,   0.0, 1.0 ,  1.0,1.0,1.0},
            { xpos + w, ypos + h,   1.0, 1.0 ,  1.0,1.0,1.0},
            { xpos + w, ypos,       1.0, 0.0 ,  1.0,1.0,1.0}
        };
        // 在四边形上渲染四边形纹理
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // 更新VBO内存的内容
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // 确保使用glBufferSubData而不是glBufferData
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // 渲染四边形
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // 准备渲染下一个字符
        x += (ch.Advance >> 6) * scale; // 按6位移位以获取像素值（1/64乘以2^6=64）
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void TextRenderer::RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, std::vector<std::vector<glm::vec3>> color)
{
    // 激活相应的渲染状态
    this->TextShader.Use();
    this->TextShader.SetInt("useGradient", 1);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(this->VAO);

    // 遍历所有字符
    std::string::const_iterator c;
    int i = 0;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = Characters[*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y + (this->Characters['H'].Bearing.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;
        // 更新每个字符的VBO
        GLfloat vertices[6][7] = {
            // 位置                 // 纹理坐标 // 颜色
            { xpos,     ypos + h,   0.0, 1.0,   color[i][2].x, color[i][2].y, color[i][2].z},
            { xpos + w, ypos,       1.0, 0.0,   color[i][1].x, color[i][1].y, color[i][1].z},
            { xpos,     ypos,       0.0, 0.0,   color[i][0].x, color[i][0].y, color[i][0].z},

            { xpos,     ypos + h,   0.0, 1.0,   color[i][2].x, color[i][2].y, color[i][2].z},
            { xpos + w, ypos + h,   1.0, 1.0,   color[i][3].x, color[i][3].y, color[i][3].z},
            { xpos + w, ypos,       1.0, 0.0,   color[i][1].x, color[i][1].y, color[i][1].z}
        };
        // 在四边形上渲染四边形纹理
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // 更新VBO内存的内容
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // 确保使用glBufferSubData而不是glBufferData
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // 渲染四边形
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // 准备渲染下一个字符
        x += (ch.Advance >> 6) * scale; // 按6位移位以获取像素值（1/64乘以2^6=64）
        i = (i + 1) % color.size();
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
