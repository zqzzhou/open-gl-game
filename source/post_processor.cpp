#include "post_processor.h"

PostProcessor::PostProcessor(Shader shader, GLuint width, GLuint height)
	: PostProcessingShader(shader), Texture(), Width(width), Height(height), Confuse(GL_FALSE), Chaos(GL_FALSE), Shake(GL_FALSE)
{
    // 初始化渲染缓冲、帧缓冲对象
    glGenFramebuffers(1, &this->MSFBO);
    glGenFramebuffers(1, &this->FBO);
    glGenRenderbuffers(1, &this->RBO);

    // 使用多重采样颜色缓冲区初始化渲染缓冲区存储（不需要深度/模板缓冲区）
    glBindFramebuffer(GL_FRAMEBUFFER, this->MSFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, this->RBO);
    GLint samples = 8;                                                                              // 样本数
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_RGB, width, height);              // 为渲染缓冲区对象分配存储
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, this->RBO);    // 将MS渲染缓冲区对象附加到帧缓冲区
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::POSTPROCESSOR: Failed to initialize MSFBO" << std::endl;

    // 将FBO、纹理初始化为blit多重采样颜色缓冲；用于着色器操作（用于后处理效果）
    glBindFramebuffer(GL_FRAMEBUFFER, this->FBO);
    this->Texture.Generate(width, height, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->Texture.ID, 0); // 将纹理作为颜色附加到帧缓冲区
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::POSTPROCESSOR: Failed to initialize FBO" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 初始化渲染数据和uniforms
    this->initRenderData();
    this->PostProcessingShader.SetInt("scene", 0, GL_TRUE);
    // chaos 和 shake 时，需要记录偏移量的数组 offset ，用于采样某像素周围的像素
    GLfloat offset = 1.0f / 300.0f;
    GLfloat offsets[9][2] = {
        { -offset,  offset  },  // 左上
        {  0.0f,    offset  },  // 上
        {  offset,  offset  },  // 右上
        { -offset,  0.0f    },  // 左
        {  0.0f,    0.0f    },  // 中
        {  offset,  0.0f    },  // 右
        { -offset, -offset  },  // 左下
        {  0.0f,   -offset  },  // 下
        {  offset, -offset  }   // 右下
    };
    PostProcessingShader.SetVec2("offsets", (GLfloat*)offsets, 9);
    // chaos 时的使用边缘锐化核
    GLint edge_kernel[9] = {
        -1, -1, -1,
        -1,  8, -1,
        -1, -1, -1
    };
    PostProcessingShader.SetIntv("edge_kernel", edge_kernel, 9);
    // shake 时的使用模糊核
    GLfloat blur_kernel[9] = {
        1.0 / 16, 2.0 / 16, 1.0 / 16,
        2.0 / 16, 4.0 / 16, 2.0 / 16,
        1.0 / 16, 2.0 / 16, 1.0 / 16
    };
    PostProcessingShader.SetFloatv("blur_kernel", blur_kernel, 9);
}

void PostProcessor::BeginRender()
{
    // 开始渲染时，将主体内容绘制至MS缓冲区
    glBindFramebuffer(GL_FRAMEBUFFER, this->MSFBO);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void PostProcessor::EndRender()
{
    // 现在将多重采样颜色缓冲区解析为中间FBO，以存储到纹理
    glBindFramebuffer(GL_READ_FRAMEBUFFER, this->MSFBO);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->FBO);
    glBlitFramebuffer(0, 0, this->Width, this->Height, 0, 0, this->Width, this->Height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // 将读写帧缓冲区绑定到默认帧缓冲区
}

void PostProcessor::Render(GLfloat time)
{
    // 设置uniforms和后处理选项
    this->PostProcessingShader.Use();
    this->PostProcessingShader.SetFloat("time", time);
    this->PostProcessingShader.SetInt("confuse", this->Confuse);
    this->PostProcessingShader.SetInt("chaos", this->Chaos);
    this->PostProcessingShader.SetInt("shake", this->Shake);
    // 绘制纹理四边形
    glActiveTexture(GL_TEXTURE0);
    this->Texture.Bind();
    glBindVertexArray(this->VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void PostProcessor::initRenderData()
{
    // 设置 VAO/VBO
    GLuint VBO;
    GLfloat vertices[] = {
        // 位置        // 纹理
        -1.0f, -1.0f, 0.0f, 0.0f,
         1.0f,  1.0f, 1.0f, 1.0f,
        -1.0f,  1.0f, 0.0f, 1.0f,

        -1.0f, -1.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 1.0f, 1.0f
    };
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(this->VAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GL_FLOAT), (GLvoid*)0);  // <vec2 position, vec2 texCoords>
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
