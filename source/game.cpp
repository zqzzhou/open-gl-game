#include "game.h"

// 游戏相关状态数据
SpriteRenderer* Renderer;                           // 精灵渲染器
GameObject* Player;                                 // 玩家
BallObject* Ball;                                   // 球
ParticleGenerator* Particles;                       // 粒子生成器
PostProcessor* Effects;                             // 特效
ISoundEngine* SoundEngine = createIrrKlangDevice(); // 声音引擎
TextRenderer* Text;                                 // 文本渲染器

GLfloat ShakeTime = 0.0f;                           // 晃动特效时间计时器

std::vector<std::vector<glm::vec3>> Textcolor;      // 字体颜色数组

Game::Game(GLuint width, GLuint height) 
    : State(GameState::GAME_MENU), Keys(), Width(width), Height(height), Lives(3), KeysProcessed(), Level(0){
}

Game::~Game()
{
    delete Renderer;
    delete Player;
    delete Ball;
    delete Particles;
    delete Effects;
    delete SoundEngine;
    delete Text;
}

void Game::Init()
{
    // 加载着色器
    ResourceManager::LoadShader("shaders/sprite.vert", "shaders/sprite.frag", nullptr, "sprite");
    ResourceManager::LoadShader("shaders/particle.vert", "shaders/particle.frag", nullptr, "particle");
    ResourceManager::LoadShader("shaders/post_processing.vert", "shaders/post_processing.frag", nullptr, "post_processing");
    // 配置着色器
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->Width),      // 正交投影
        static_cast<GLfloat>(this->Height), 0.0f, -1.0f, 1.0f);
    ResourceManager::GetShader("sprite").Use().SetInt("image", 0);
    ResourceManager::GetShader("sprite").SetMat4("projection", projection);
    ResourceManager::GetShader("particle").Use().SetInt("sprite", 0);
    ResourceManager::GetShader("particle").SetMat4("projection", projection);
    // 加载纹理
    ResourceManager::LoadTexture("resources/textures/background.jpg", GL_FALSE, "background");
    ResourceManager::LoadTexture("resources/textures/paddle.png", GL_TRUE, "paddle");
    ResourceManager::LoadTexture("resources/textures/awesomeface.png", GL_TRUE, "face");
    ResourceManager::LoadTexture("resources/textures/block.png", GL_FALSE, "block");
    ResourceManager::LoadTexture("resources/textures/block_solid.png", GL_FALSE, "block_solid");
    ResourceManager::LoadTexture("resources/textures/particle.png", GL_TRUE, "particle");
    ResourceManager::LoadTexture("resources/textures/powerup_chaos.png", GL_TRUE, "powerup_chaos");
    ResourceManager::LoadTexture("resources/textures/powerup_confuse.png", GL_TRUE, "powerup_confuse");
    ResourceManager::LoadTexture("resources/textures/powerup_increase.png", GL_TRUE, "powerup_increase");
    ResourceManager::LoadTexture("resources/textures/powerup_passthrough.png", GL_TRUE, "powerup_passthrough");
    ResourceManager::LoadTexture("resources/textures/powerup_speed.png", GL_TRUE, "powerup_speed");
    ResourceManager::LoadTexture("resources/textures/powerup_sticky.png", GL_TRUE, "powerup_sticky");
    // 设置专用于渲染的控制
    Renderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));
    Particles = new ParticleGenerator(ResourceManager::GetShader("particle"), ResourceManager::GetTexture("particle"), 500);
    Effects = new PostProcessor(ResourceManager::GetShader("post_processing"), this->Width, this->Height);
    Text = new TextRenderer(this->Width, this->Height);
    // 加载关卡
    GameLevel one; one.Load("resources/levels/one.lvl", this->Width, this->Height * 0.5);
    GameLevel two; two.Load("resources/levels/two.lvl", this->Width, this->Height * 0.5);
    GameLevel three; three.Load("resources/levels/three.lvl", this->Width, this->Height * 0.5);
    GameLevel four; four.Load("resources/levels/four.lvl", this->Width, this->Height * 0.5);
    this->Levels.emplace_back(one);
    this->Levels.emplace_back(two);
    this->Levels.emplace_back(three);
    this->Levels.emplace_back(four);
    this->Level = 0;
    // 配置游戏对象
    glm::vec2 playerPos = glm::vec2(this->Width / 2 - PLAYER_SIZE.x / 2, this->Height - PLAYER_SIZE.y); // 挡板左上角顶点
    Player = new GameObject(playerPos, PLAYER_SIZE, ResourceManager::GetTexture("paddle"));
    glm::vec2 ballPos = playerPos + glm::vec2(PLAYER_SIZE.x / 2 - BALL_RADIUS, -BALL_RADIUS * 2);       // 圆外接四边形左上角顶点
    Ball = new BallObject(ballPos, BALL_RADIUS, INITIAL_BALL_VELOCITY, ResourceManager::GetTexture("face"));
    // 加载游戏音乐
    SoundEngine->play2D("resources/audio/breakout.mp3", GL_TRUE);
    // 加载字形
    Text->Load("resources/fonts/ocraext.TTF", 24);
    // 字体颜色控制
    std::vector<glm::vec3> color1 = { glm::vec3(1.0,0.0,0.0),glm::vec3(0.0,1.0,0.0), glm::vec3(0.0,0.0,1.0), glm::vec3(1.0,0.0,1.0) };
    std::vector<glm::vec3> color2 = { glm::vec3(0.0,1.0,0.0),glm::vec3(0.0,0.0,1.0), glm::vec3(1.0,0.0,0.0), glm::vec3(0.0,1.0,1.0) };
    std::vector<glm::vec3> color3 = { glm::vec3(0.0,0.0,1.0),glm::vec3(1.0,0.0,0.0), glm::vec3(0.0,1.0,0.0), glm::vec3(1.0,1.0,0.0) };
    std::vector<glm::vec3> color4 = { glm::vec3(1.0,0.0,1.0),glm::vec3(1.0,1.0,0.0), glm::vec3(0.0,1.0,1.0), glm::vec3(1.0,1.0,1.0) };
    std::vector<glm::vec3> color5 = { glm::vec3(1.0,1.0,1.0),glm::vec3(0.0,1.0,0.5), glm::vec3(0.0,0.5,1.0), glm::vec3(1.0,0.5,1.0) };
    Textcolor.emplace_back(color1);
    Textcolor.emplace_back(color2);
    Textcolor.emplace_back(color3);
    Textcolor.emplace_back(color4);
    Textcolor.emplace_back(color5);
}

void Game::ProcessInput(GLfloat dt)
{
    if (this->State == GameState::GAME_MENU)
    {
        // 游戏菜单
        if (this->Keys[GLFW_KEY_ENTER] && !this->KeysProcessed[GLFW_KEY_ENTER]) 
        {
            this->State = GameState::GAME_ACTIVE;
            this->KeysProcessed[GLFW_KEY_ENTER] = GL_TRUE;  // 限制按键只能单点，不能长按
        }
        if (this->Keys[GLFW_KEY_W] && !this->KeysProcessed[GLFW_KEY_W])
        {
            this->Level = (this->Level + 1) % 4;
            this->KeysProcessed[GLFW_KEY_W] = GL_TRUE;
        }
        if (this->Keys[GLFW_KEY_S] && !this->KeysProcessed[GLFW_KEY_S])
        {
            if (this->Level > 0)
                --this->Level;
            else
                this->Level = 3;
            this->KeysProcessed[GLFW_KEY_S] = GL_TRUE;
        }
    }
    if (this->State == GameState::GAME_ACTIVE)
    {
        // 游戏开始
        GLfloat velocity = PLAYER_VELOCITY * dt;
        // 移动挡板
        if (this->Keys[GLFW_KEY_A])
        {
            if (Player->Position.x >= 0)
            {
                Player->Position.x -= velocity;
                if (Ball->Stuck)    // 球还未发射前或者球粘粘在挡板上时，球跟着玩家操作挡板一起运动
                    Ball->Position.x -= velocity;
            }
        }
        if (this->Keys[GLFW_KEY_D])
        {
            if (Player->Position.x <= this->Width - Player->Size.x)
            {
                Player->Position.x += velocity;
                if (Ball->Stuck)
                    Ball->Position.x += velocity;
            }
        }
        if (this->Keys[GLFW_KEY_SPACE]) // 空格发射球
            Ball->Stuck = false;
    }
    if (this->State == GameState::GAME_WIN)
    {
        // 游戏胜利
        if (this->Keys[GLFW_KEY_ENTER])
        {
            this->KeysProcessed[GLFW_KEY_ENTER] = GL_TRUE;
            Effects->Chaos = GL_FALSE;  // 取消黑屏效果
            this->State = GameState::GAME_MENU;
        }
    }
}

void Game::Update(GLfloat dt)
{
    // 更新对象
    glm::vec2 BallLastPosition = Ball->Move(dt, this->Width);   // 获得上次 Update 时球的位置
    // 检测碰撞
    this->DoCollisions(BallLastPosition);
    // 更新粒子
    Particles->Update(dt, *Ball, 2, glm::vec2(Ball->Radius / 2));
    // 更新道具
    this->UpdatePowerUps(dt);
    // 减少Shake效果时间
    if (ShakeTime > 0.0f)
    {
        ShakeTime -= dt;
        if (ShakeTime <= 0.0f)
            Effects->Shake = GL_FALSE;
    }
    // 检查失败条件
    if (Ball->Position.y >= this->Height) // 球是否接触底部边界？
    {
        // 玩家生命减少
        --this->Lives;
        // 玩家失去生命时，结束惩罚效果
        Effects->Confuse = GL_FALSE;
        Effects->Chaos = GL_FALSE;
        // 玩家是否已失去所有生命值? : 游戏结束
        if (this->Lives == 0)
        {
            this->ResetLevel();
            this->State = GameState::GAME_MENU;
        }
        this->ResetPlayer();
    }
    // 检查胜利条件
    if (this->State == GameState::GAME_ACTIVE && this->Levels[this->Level].IsCompleted())
    {
        this->ResetLevel();
        this->ResetPlayer();
        Effects->Chaos = GL_TRUE;
        this->State = GameState::GAME_WIN;
    }
}

void Game::Render()
{
    Texture2D background = ResourceManager::GetTexture("background");
    if (this->State == GameState::GAME_ACTIVE || this->State == GameState::GAME_MENU)
    {
        // 开始渲染到后处理四边形
        Effects->BeginRender();
        // 绘制背景
        Renderer->DrawSprite(background,
            glm::vec2(0, 0), glm::vec2(this->Width, this->Height), 0.0f
        );
        // 绘制关卡
        this->Levels[this->Level].Draw(*Renderer);
        // 绘制挡板
        Player->Draw(*Renderer);
        // 绘制粒子
        Particles->Draw();
        // 绘制球
        Ball->Draw(*Renderer);
        // 绘制道具
        for (PowerUp& powerUp : this->PowerUps)
            if (!powerUp.Destroyed)
                powerUp.Draw(*Renderer);
        // 结束渲染到后处理四边形
        Effects->EndRender();
        // 绘制后处理四边形
        Effects->Render(glfwGetTime());
        // 绘制文本
        std::stringstream ss; ss << this->Lives;
        
        Text->RenderText("Lives:" + ss.str(), 5.0f, 5.0f, 1.0f, Textcolor);
    }
    if (this->State == GameState::GAME_MENU)
    {
        Text->RenderText("Press ENTER to start", 250.0f, Height / 2, 1.0f, Textcolor);
        Text->RenderText("Press W or S to select level", 245.0f, Height / 2 + 20.0f, 0.75f, Textcolor);
    }
    if (this->State == GameState::GAME_WIN)
    {
        Text->RenderText(
            "You WON!!!", 320.0, Height / 2 - 20.0, 1.0, glm::vec3(0.0, 1.0, 0.0)
        );
        Text->RenderText(
            "Press ENTER to retry or ESC to quit", 130.0, Height / 2, 1.0, glm::vec3(1.0, 1.0, 0.0)
        );
    }
}
// 渲染帧率文本
void Game::RenderFrame(GLfloat dt) {
    // 绘制文本
    std::stringstream ss; ss << (int)(1.f / dt + 0.5f);
    Text->RenderText("Frame Rate:" + ss.str(), this->Width - 100.0f, 5.0f, 0.5f, glm::vec3(0.f, 1.f, 0.f));
}
// 将碰撞矢量转换为碰撞方向
Direction VectorDirection(glm::vec2 target)
{
    glm::vec2 compass[] = {
        glm::vec2(0.0f, 1.0f),  // 上
        glm::vec2(1.0f, 0.0f),  // 右
        glm::vec2(0.0f, -1.0f), // 下
        glm::vec2(-1.0f, 0.0f)  // 左
    };
    GLfloat max = 0.0f;
    GLuint best_match = -1;
    for (GLuint i = 0; i < 4; i++)
    {
        // 若碰撞矢量为正斜矢量，则（+，+）和（-,+）上，（+，-）右，（-，-）下
        GLfloat dot_product = glm::dot(glm::normalize(target), compass[i]);
        if (dot_product > max)
        {
            max = dot_product;
            best_match = i;
        }
    }
    return (Direction)best_match;
}

GLboolean CheckCollision(GameObject& one, GameObject& two) // AABB - AABB collision
{
    // x轴方向碰撞？
    bool collisionX = one.Position.x + one.Size.x >= two.Position.x &&
        two.Position.x + two.Size.x >= one.Position.x;
    // y轴方向碰撞？
    bool collisionY = one.Position.y + one.Size.y >= two.Position.y &&
        two.Position.y + two.Size.y >= one.Position.y;
    // 只有两个轴向都有碰撞时才碰撞
    return collisionX && collisionY;
}

Collision CheckCollision(BallObject& one, GameObject& two, glm::vec2 ballLastPosition) // AABB - Circle collision
{
    bool isCollison = GL_FALSE;
    // 获取圆的中心 
    glm::vec2 center(one.Position + one.Radius);
    // 计算AABB的信息（中心、半边长）
    glm::vec2 aabb_half_extents(two.Size.x / 2, two.Size.y / 2);
    glm::vec2 aabb_center(
        two.Position.x + aabb_half_extents.x,
        two.Position.y + aabb_half_extents.y
    );
    // 获取两个中心的差矢量
    glm::vec2 difference = center - aabb_center;
    // 根据AABB的半边长将 difference 矢量限制在(-aabb_half_extents, aabb_half_extents)内
    glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
    // AABB_center加上clamped这样就得到了碰撞箱上距离圆最近的点closest
    glm::vec2 closest = aabb_center + clamped;
    // 获得圆心center和最近点closest的矢量并判断其 length 是否<= radius
    difference = closest - center;
    if (glm::length(difference) <= one.Radius)
        isCollison = GL_TRUE;
    
    // 上次 Update 与这次 Update 之间对中间插值作碰撞检测
    glm::vec2 middle = glm::vec2((one.Position.x + ballLastPosition.x) / 2, (one.Position.y + ballLastPosition.y) / 2);
    glm::vec2 m_center = middle + one.Radius;
    glm::vec2 m_difference = m_center - aabb_center;
    glm::vec2 m_clamped = glm::clamp(m_difference, -aabb_half_extents, aabb_half_extents);
    glm::vec2 m_closest = aabb_center + m_clamped;
    m_difference = m_closest - m_center;
    if (glm::length(m_difference) <= one.Radius)
        isCollison = GL_TRUE;

    if (isCollison) {
        // 返回是否发生碰撞，碰撞方向，圆心与碰撞点之间的矢量组成的三元组
        if (glm::length(difference) == 0)
            return std::make_tuple(GL_TRUE, VectorDirection(m_difference), m_difference);
        return std::make_tuple(GL_TRUE, VectorDirection(difference), difference);
    }
    else
        return std::make_tuple(GL_FALSE, Direction::DIR_UP, glm::vec2(0, 0));
}

void ActivatePowerUp(PowerUp& powerUp)
{
    // 根据道具类型发动道具
    if (powerUp.Type == "speed")
    {
        Ball->Velocity *= 1.2;
    }
    else if (powerUp.Type == "sticky")
    {
        Ball->Sticky = GL_TRUE;
        Player->Color = glm::vec3(1.0f, 0.5f, 1.0f);
    }
    else if (powerUp.Type == "pass-through")
    {
        Ball->PassThrough = GL_TRUE;
        Ball->Color = glm::vec3(1.0f, 0.5f, 0.5f);
    }
    else if (powerUp.Type == "pad-size-increase")
    {
        Player->Size.x += 50;
    }
    else if (powerUp.Type == "confuse")
    {
        if (!Effects->Chaos)
            Effects->Confuse = GL_TRUE; // 只在chaos未激活时生效，chaos同理
    }
    else if (powerUp.Type == "chaos")
    {
        if (!Effects->Confuse)
            Effects->Chaos = GL_TRUE;
    }
}

void Game::DoCollisions(glm::vec2 ballLastPosition)
{
    // 循环每块砖做碰撞检测（从后往前，即从右下往左上）
    for (GameObject& box : this->Levels[this->Level].Bricks)
    {
        // 如果该砖被已被破坏则不做检测
        if (!box.Destroyed)
        {
            // 检测该砖与球的距离，距离较远则检测下一块砖
            glm::vec2 center(Ball->Position + Ball->Radius);
            glm::vec2 aabb_center(
                box.Position.x + box.Size.x / 2,
                box.Position.y + box.Size.y / 2
            );
            if (glm::length(center - aabb_center) >  Ball->Radius + box.Size.x +  box.Size.y)
                continue;
            // 进行碰撞检测
            Collision collision = CheckCollision(*Ball, box, ballLastPosition);
            // 发送碰撞
            if (std::get<0>(collision)) // 如果 collision 是 true
            {
                // 如果砖块不是实心就销毁砖块
                if (!box.IsSolid) {
                    box.Destroyed = GL_TRUE;
                    this->SpawnPowerUps(box);   // 概率掉落道具
                    SoundEngine->play2D("resources/audio/bleep.mp3", GL_FALSE);
                }
                else {
                    // 如果是实心的砖块则激活shake特效
                    ShakeTime = 0.05f;
                    Effects->Shake = GL_TRUE;      // 激活震动特效
                    SoundEngine->play2D("resources/audio/solid.wav", GL_FALSE);
                }
                // 碰撞处理
                Direction dir = std::get<1>(collision);
                glm::vec2 diff_vector = std::get<2>(collision);
                if (!(Ball->PassThrough && !box.IsSolid))
                {
                    if (dir == Direction::DIR_LEFT || dir == Direction::DIR_RIGHT) // 水平方向碰撞
                    {
                        Ball->Velocity.x = -Ball->Velocity.x; // 反转水平速度
                        // 重定位
                        GLfloat penetration = Ball->Radius - std::abs(diff_vector.x);
                        if (dir == Direction::DIR_LEFT)
                            Ball->Position.x += penetration; // 将球右移
                        else
                            Ball->Position.x -= penetration; // 将球左移
                    }
                    else // 垂直方向碰撞
                    {
                        Ball->Velocity.y = -Ball->Velocity.y; // 反转垂直速度
                        // 重定位
                        GLfloat penetration = Ball->Radius - std::abs(diff_vector.y);
                        if (dir == Direction::DIR_UP)
                            Ball->Position.y -= penetration; // 将球上移
                        else
                            Ball->Position.y += penetration; // 将球下移
                    }
                }
                // 每次 Update 只对一个砖块进行一次碰撞检测和处理，以保证不会出现两块砖同时消失导致碰撞处理失效的情况
                break;
            }
        }
    }
    Collision result = CheckCollision(*Ball, *Player, ballLastPosition);
    if (!Ball->Stuck && std::get<0>(result))
    {
        // 检查碰到了挡板的哪个位置，并根据碰到哪个位置来改变速度
        GLfloat centerBoard = Player->Position.x + Player->Size.x / 2;
        GLfloat distance = (Ball->Position.x + Ball->Radius) - centerBoard;
        GLfloat percentage = distance / (Player->Size.x / 2);
        // 依据结果移动
        GLfloat strength = 2.0f;
        glm::vec2 oldVelocity = Ball->Velocity;
        Ball->Velocity.x = INITIAL_BALL_VELOCITY.x * percentage * strength;
        //Ball->Velocity.y = -Ball->Velocity.y; 
        Ball->Velocity.y = -1 * abs(Ball->Velocity.y);  // 处理粘板问题(Sticky Paddle Issue)
        Ball->Velocity = glm::normalize(Ball->Velocity) * glm::length(oldVelocity);
        Ball->Stuck = Ball->Sticky;                     // 如果有粘粘道具效果则球粘粘在挡板上
        SoundEngine->play2D("resources/audio/bleep.wav", GL_FALSE);
    }
    for (PowerUp& powerUp : this->PowerUps)
    {
        if (!powerUp.Destroyed)
        {
            if (powerUp.Position.y >= this->Height)
                powerUp.Destroyed = GL_TRUE;
            if (CheckCollision(*Player, powerUp))
            {   // 道具与挡板接触，激活它！
                ActivatePowerUp(powerUp);
                powerUp.Destroyed = GL_TRUE;
                powerUp.Activated = GL_TRUE;
                SoundEngine->play2D("resources/audio/powerup.wav", GL_FALSE);
            }
        }
    }
}

void Game::ResetLevel()
{
    if (this->Level == 0)this->Levels[0].Load("resources/levels/one.lvl", this->Width, this->Height * 0.5f);
    else if (this->Level == 1)
        this->Levels[1].Load("resources/levels/two.lvl", this->Width, this->Height * 0.5f);
    else if (this->Level == 2)
        this->Levels[2].Load("resources/levels/three.lvl", this->Width, this->Height * 0.5f);
    else if (this->Level == 3)
        this->Levels[3].Load("resources/levels/four.lvl", this->Width, this->Height * 0.5f);
    this->Lives = 3;
}

void Game::ResetPlayer()
{
    // 重置挡板/球的状态
    Player->Size = PLAYER_SIZE;
    Player->Position = glm::vec2(this->Width / 2 - PLAYER_SIZE.x / 2, this->Height - PLAYER_SIZE.y);
    Ball->Reset(Player->Position + glm::vec2(PLAYER_SIZE.x / 2 - BALL_RADIUS, -(BALL_RADIUS * 2)), INITIAL_BALL_VELOCITY);
}

GLboolean ShouldSpawn(GLuint chance)
{
    GLuint random = rand() % chance;
    return random == 0;
}

void Game::SpawnPowerUps(GameObject& block)
{
    if (ShouldSpawn(75)) // 1/75的几率
        this->PowerUps.emplace_back(
            PowerUp("speed", glm::vec3(0.5f, 0.5f, 1.0f), 0.0f, block.Position, ResourceManager::GetTexture("powerup_speed")
            ));
    if (ShouldSpawn(75))
        this->PowerUps.emplace_back(
            PowerUp("sticky", glm::vec3(1.0f, 0.5f, 1.0f), 20.0f, block.Position, ResourceManager::GetTexture("powerup_sticky")
            ));
    if (ShouldSpawn(75))
        this->PowerUps.emplace_back(
            PowerUp("pass-through", glm::vec3(0.5f, 1.0f, 0.5f), 10.0f, block.Position, ResourceManager::GetTexture("powerup_passthrough")
            ));
    if (ShouldSpawn(75))
        this->PowerUps.emplace_back(
            PowerUp("pad-size-increase", glm::vec3(1.0f, 0.6f, 0.4), 0.0f, block.Position, ResourceManager::GetTexture("powerup_increase")
            ));
    if (ShouldSpawn(15)) // 负面道具被更频繁地生成
        this->PowerUps.emplace_back(
            PowerUp("confuse", glm::vec3(1.0f, 0.3f, 0.3f), 15.0f, block.Position, ResourceManager::GetTexture("powerup_confuse")
            ));
    if (ShouldSpawn(15))
        this->PowerUps.emplace_back(
            PowerUp("chaos", glm::vec3(0.9f, 0.25f, 0.25f), 15.0f, block.Position, ResourceManager::GetTexture("powerup_chaos")
            ));
}

GLboolean IsOtherPowerUpActive(std::vector<PowerUp>& powerUps, std::string type)
{
    for (const PowerUp& powerUp : powerUps)
    {
        if (powerUp.Activated)
            if (powerUp.Type == type)
                return GL_TRUE;
    }
    return GL_FALSE;
}

void Game::UpdatePowerUps(GLfloat dt)
{
    for (PowerUp& powerUp : this->PowerUps)
    {
        powerUp.Position += powerUp.Velocity * dt;
        if (powerUp.Activated)
        {
            powerUp.Duration -= dt;

            if (powerUp.Duration <= 0.0f)
            {
                // 之后会将这个道具移除
                powerUp.Activated = GL_FALSE;
                // 停用效果
                if (powerUp.Type == "sticky")
                {
                    if (!IsOtherPowerUpActive(this->PowerUps, "sticky"))
                    {   // 仅当没有其他sticky效果处于激活状态时重置，以下同理
                        Ball->Sticky = GL_FALSE;
                        Player->Color = glm::vec3(1.0f);
                    }
                }
                else if (powerUp.Type == "pass-through")
                {
                    if (!IsOtherPowerUpActive(this->PowerUps, "pass-through"))
                    {
                        Ball->PassThrough = GL_FALSE;
                        Ball->Color = glm::vec3(1.0f);
                    }
                }
                else if (powerUp.Type == "confuse")
                {
                    if (!IsOtherPowerUpActive(this->PowerUps, "confuse"))
                    {
                        Effects->Confuse = GL_FALSE;
                    }
                }
                else if (powerUp.Type == "chaos")
                {
                    if (!IsOtherPowerUpActive(this->PowerUps, "chaos"))
                    {
                        Effects->Chaos = GL_FALSE;
                    }
                }
            }
        }
    }
    this->PowerUps.erase(std::remove_if(this->PowerUps.begin(), this->PowerUps.end(),
        [](const PowerUp& powerUp) { return powerUp.Destroyed && !powerUp.Activated; }
    ), this->PowerUps.end());
}

