#include "shader.h"

Shader& Shader::Use()
{
    glUseProgram(this->ID);
    return *this;
}

void Shader::Compile(const GLchar* vertexSource, const GLchar* fragmentSource, const GLchar* geometrySource)
{
    GLuint sVertex, sFragment, gShader;
    // 顶点着色器
    sVertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(sVertex, 1, &vertexSource, NULL);
    glCompileShader(sVertex);
    checkCompileErrors(sVertex, "VERTEX");
    // 片段着色器
    sFragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(sFragment, 1, &fragmentSource, NULL);
    glCompileShader(sFragment);
    checkCompileErrors(sFragment, "FRAGMENT");
    // 如果存在几何着色器路径，同时编译几何着色器
    if (geometrySource != nullptr)
    {
        gShader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(gShader, 1, &geometrySource, NULL);
        glCompileShader(gShader);
        checkCompileErrors(gShader, "GEOMETRY");
    }
    // 着色器程序
    this->ID = glCreateProgram();
    glAttachShader(this->ID, sVertex);
    glAttachShader(this->ID, sFragment);
    if (geometrySource != nullptr)
        glAttachShader(this->ID, gShader);
    glLinkProgram(this->ID);
    checkCompileErrors(this->ID, "PROGRAM");
    // 当着色器链接到程序之后，删除不再需要的着色器
    glDeleteShader(sVertex);
    glDeleteShader(sFragment);
    if (geometrySource != nullptr)
        glDeleteShader(gShader);
}

void Shader::SetFloat(const GLchar* name, GLfloat value, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform1f(glGetUniformLocation(this->ID, name), value);
}

void Shader::SetFloatv(const GLchar* name, GLfloat* value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform1fv(glGetUniformLocation(this->ID, name), size, value);
}

void Shader::SetInt(const GLchar* name, GLint value, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform1i(glGetUniformLocation(this->ID, name), value);
}

void Shader::SetIntv(const GLchar* name, GLint* value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform1iv(glGetUniformLocation(this->ID, name), size, value);
}

void Shader::SetVec2(const GLchar* name, GLfloat x, GLfloat y, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform2f(glGetUniformLocation(this->ID, name), x, y);
}

void Shader::SetVec2(const GLchar* name, const glm::vec2& value, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform2fv(glGetUniformLocation(this->ID, name), 1, &value[0]);
}

void Shader::SetVec2(const GLchar* name, const glm::vec2& value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform2fv(glGetUniformLocation(this->ID, name), size, &value[0]);
}

void Shader::SetVec2(const GLchar* name, GLfloat* value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform2fv(glGetUniformLocation(this->ID, name), size, value);
}

void Shader::SetVec3(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform3f(glGetUniformLocation(this->ID, name), x, y, z);
}

void Shader::SetVec3(const GLchar* name, const glm::vec3& value, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform3fv(glGetUniformLocation(this->ID, name), 1, &value[0]);
}

void Shader::SetVec3(const GLchar* name, const glm::vec3& value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform3fv(glGetUniformLocation(this->ID, name), size, &value[0]);
}

void Shader::SetVec4(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform4f(glGetUniformLocation(this->ID, name), x, y, z, w);
}

void Shader::SetVec4(const GLchar* name, const glm::vec4& value, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform4fv(glGetUniformLocation(this->ID, name), 1, &value[0]);
}

void Shader::SetVec4(const GLchar* name, const glm::vec4& value, GLsizei size, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniform4fv(glGetUniformLocation(this->ID, name), size, &value[0]);
}

void Shader::SetMat2(const GLchar* name, const glm::mat2& matrix, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniformMatrix2fv(glGetUniformLocation(this->ID, name), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::SetMat3(const GLchar* name, const glm::mat3& matrix, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniformMatrix3fv(glGetUniformLocation(this->ID, name), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::SetMat4(const GLchar* name, const glm::mat4& matrix, GLboolean useShader)
{
    if (useShader)
        this->Use();
    glUniformMatrix4fv(glGetUniformLocation(this->ID, name), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::checkCompileErrors(GLuint object, std::string type)
{
    GLint success;
    GLchar infoLog[1024];
    if (type != "PROGRAM")
    {
        glGetShaderiv(object, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(object, 1024, NULL, infoLog);
            std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << "\n"
                << infoLog << "\n -- --------------------------------------------------- -- "
                << std::endl;
        }
    }
    else
    {
        glGetProgramiv(object, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(object, 1024, NULL, infoLog);
            std::cout << "| ERROR::Shader: Link-time error: Type: " << type << "\n"
                << infoLog << "\n -- --------------------------------------------------- -- "
                << std::endl;
        }
    }
}
