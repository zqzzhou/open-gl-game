#include "particle_generator.h"

ParticleGenerator::ParticleGenerator(Shader shader, Texture2D texture, GLuint amount)
	: shader(shader), texture(texture), amount(amount)
{
	this->init();
}

void ParticleGenerator::Update(GLfloat dt, GameObject& object, GLuint newParticleNum, glm::vec2 offset)
{
    // 添加新的粒子
    for (GLuint i = 0; i < newParticleNum; ++i)
    {
        int unusedParticle = this->firstUnusedParticle();
        this->respawnParticle(this->particles[unusedParticle], object, offset);
    }
    // 更新所有粒子
    for (GLuint i = 0; i < this->amount; ++i)
    {
        Particle& p = this->particles[i];
        p.Life -= dt; // 生命周期减少
        if (p.Life > 0.0f)
        {	// 粒子存活，更新其位置和颜色透明度（透明度减少作为粒子渐渐消失的效果）
            p.Position -= p.Velocity * dt;
            p.Color.a -= dt * 2.5;
        }
    }
}

void ParticleGenerator::Draw()
{
    // 使用额外的混合使其产生发光效果
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    this->shader.Use();
    for (Particle &particle : this->particles)
    {
        if (particle.Life > 0.0f)
        {
            this->shader.SetVec2("offset", particle.Position);
            this->shader.SetVec4("color", particle.Color);
            this->texture.Bind();
            glBindVertexArray(this->VAO);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindVertexArray(0);
        }
    }
    // 重置混合模式
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void ParticleGenerator::init()
{
    GLuint VBO;
    GLfloat particle_quad[] = {
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
    };
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(this->VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glBindVertexArray(0);

    // 创建 amount 个粒子实例
    for (GLuint i = 0; i < this->amount; ++i)
        this->particles.push_back(Particle());
}

// 存储最后一个使用的粒子(以便快速访问到下一个已死亡的粒子)
GLuint lastUsedParticle = 0;
GLuint ParticleGenerator::firstUnusedParticle()
{
    // 首先搜索最后一个使用的粒子, 这通常马上就能返回
    for (GLuint i = lastUsedParticle; i < this->amount; ++i) {
        if (this->particles[i].Life <= 0.0f) {
            lastUsedParticle = i;
            return i;
        }
    }
    // 否则作线性搜索
    for (GLuint i = 0; i < lastUsedParticle; ++i) {
        if (this->particles[i].Life <= 0.0f) {
            lastUsedParticle = i;
            return i;
        }
    }
    // 当所有粒子都被取过后，重置最后使用过的粒子为0 (请注意，如果它反复碰到这种情况，则应保留更多粒子)
    lastUsedParticle = 0;
    return 0;
}

void ParticleGenerator::respawnParticle(Particle& particle, GameObject& object, glm::vec2 offset)
{
    GLfloat random = ((rand() % 100) - 50) / 10.0f;
    GLfloat rColor = 0.5f + ((rand() % 100) / 100.0f);
    GLfloat gColor = 0.5f + ((rand() % 100) / 100.0f);
    GLfloat bColor = 0.5f + ((rand() % 100) / 100.0f);
    //GLfloat rColor = 0;
    //GLfloat gColor = 1.0f;
    //GLfloat bColor = 0;
    particle.Position = object.Position + random + offset;
    particle.Color = glm::vec4(rColor, gColor, bColor, 1.0f);
    particle.Life = 0.5f;
    particle.Velocity = object.Velocity * 0.1f;
}
