#pragma once
#ifndef PARTICLE_GENERATOR_H
#define PARTICLE_GENERATOR_H

#include <vector>

#include "shader.h"
#include "texture.h"
#include "game_object.h"

// 粒子
struct Particle {

    glm::vec2 Position, Velocity;   // 位置和速度
    glm::vec4 Color;                // 颜色
    GLfloat Life;                   // 生命周期

    Particle()
        : Position(0.0f), Velocity(0.0f), Color(1.0f), Life(0.0f) { }
};

class ParticleGenerator
{
public:
    // 构造函数
    ParticleGenerator(Shader shader, Texture2D texture, GLuint amount);
    // 更新所有粒子
    void Update(GLfloat dt, GameObject& object, GLuint newParticleNum, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
    // 绘制所有粒子
    void Draw();
private:
    // 状态
    std::vector<Particle> particles;
    GLuint amount;
    // 渲染状态
    Shader shader;
    Texture2D texture;
    GLuint VAO;
    // 初始化顶点缓冲区和顶点数组
    void init();
    // 返回当前未使用的第一个粒子索引，例如生命<=0.0f或如果当前没有粒子处于非活动状态，则返回0
    GLuint firstUnusedParticle();
    // 重生粒子
    void respawnParticle(Particle& particle, GameObject& object, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
};

#endif