#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>

class Texture2D
{
public:
    // 保存纹理对象的ID，用于引用此特殊纹理的所有纹理操作
    GLuint ID;
    // 纹理大小
    GLuint Width, Height;   // 加载图像的宽度和高度（以像素为单位）
    // 纹理格式
    GLuint Internal_Format; // 纹理对象的格式
    GLuint Image_Format;    // 加载图像的格式
    // 纹理配置
    GLuint Wrap_S;          // S轴的环绕模式
    GLuint Wrap_T;          // T轴的环绕模式
    GLuint Filter_Min;      // 纹素<屏幕像素时的过滤模式
    GLuint Filter_Max;      // 纹素>屏幕像素时的过滤模式
    // 构造函数 (设置默认纹理模式)
    Texture2D();
    // 从图像中生成纹理
    void Generate(GLuint width, GLuint height, unsigned char* data);
    // 将纹理绑定为当前活动的 GL_TEXTURE_2D 纹理对象
    void Bind() const;
};

#endif