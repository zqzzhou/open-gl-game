#pragma once
#ifndef TEXT_RENDERER_H
#define TEXT_RENDERER_H

#include <map>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "shader.h"
#include "resource_manager.h"
#include <vector>

// 保存与使用FreeType加载的角色相关的所有状态信息
struct Character {
    GLuint TextureID;   // 字形纹理的ID句柄
    glm::ivec2 Size;    // 字形的大小
    glm::ivec2 Bearing; // 从基线到图示符左侧/顶部的偏移
    GLuint Advance;     // 到下一个图示符的水平偏移
};

class TextRenderer
{
public:
    // 保存预编译字符的列表
    std::map<GLchar, Character> Characters;
    // 用于文本渲染的着色器
    Shader TextShader;
    // 构造函数
    TextRenderer(GLuint width, GLuint height);
    // 预编译给定字体的字符列表
    void Load(std::string font, GLuint fontSize);
    // 使用预编译的字符列表呈现文本字符串
    void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color = glm::vec3(1.0f));
    // 使用预编译的字符列表呈现文本字符串(字体颜色可变)
    void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, std::vector<std::vector<glm::vec3>> color);
private:
    // 渲染状态
    GLuint VAO, VBO;
};

#endif