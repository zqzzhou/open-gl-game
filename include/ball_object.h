#pragma once
#ifndef BALL_OBJECT_H
#define BALL_OBJECT_H

#include "game_object.h"

class BallObject : public GameObject
{
public:
    // ���״̬ 
    GLfloat   Radius;               // ��İ뾶
    GLboolean Stuck;                // �Ƿ�ճճ�ڵ����ϣ����ƿ��֣�
    GLboolean Sticky, PassThrough;  // ճճЧ������͸Ч���Ƿ񼤻�

    BallObject();
    BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite);

    glm::vec2 Move(GLfloat dt, GLuint window_width);            // �ƶ�
    void      Reset(glm::vec2 position, glm::vec2 velocity);    // ����
};

#endif
