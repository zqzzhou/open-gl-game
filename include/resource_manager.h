#pragma once
#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include <glad/glad.h>
#include "stb_image.h"
#include "texture.h"
#include "shader.h"

class ResourceManager
{
public:
    // 资源存储
    static std::map<std::string, Shader>    Shaders;
    static std::map<std::string, Texture2D> Textures;
    // 从顶点、片段（和几何）着色器源代码的文件中加载（并生成）着色器程序。如果gShaderFile不是nullptr，它还会加载一个几何着色器
    static Shader   LoadShader(const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile, std::string name);
    // 获取一个现有的着色器
    static Shader   GetShader(std::string name);
    // 从文件中加载（和生成）一个纹理
    static Texture2D LoadTexture(const GLchar* file, GLboolean alpha, std::string name);
    // 获取一个现有的纹理
    static Texture2D GetTexture(std::string name);
    // 正确地取消分配所有加载的资源
    static void      Clear();
private:
    // 私有构造函数，也就是说，我们不需要任何实际的资源管理器对象。其成员和函数应公开（静态）
    ResourceManager() { }
    // 从文件中加载和生成着色器
    static Shader    loadShaderFromFile(const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile = nullptr);
    // 从文件中加载和生成一个纹理
    static Texture2D loadTextureFromFile(const GLchar* file, GLboolean alpha);
};

#endif