#pragma once
#ifndef GAME_H
#define GAME_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <irrKlang.h>
using namespace irrklang;

#include "sprite_renderer.h"
#include "resource_manager.h"
#include "game_level.h"
#include "ball_object.h"
#include "particle_generator.h"
#include "post_processor.h"
#include "power_up.h"
#include "text_renderer.h"

// 代表了游戏的当前状态
enum class GameState {
    GAME_ACTIVE,
    GAME_MENU,
    GAME_WIN
};
// 碰撞方向
enum class Direction {
    DIR_UP,
    DIR_RIGHT,
    DIR_DOWN,
    DIR_LEFT
};

// 碰撞相关数据
typedef std::tuple<GLboolean, Direction, glm::vec2> Collision;

// 初始化挡板的大小
const glm::vec2 PLAYER_SIZE(100, 20);
// 初始化挡板的速率
const GLfloat PLAYER_VELOCITY(500.0f);
// 初始化球的速度
const glm::vec2 INITIAL_BALL_VELOCITY(100.0f, -350.0f);
// 球的半径
const GLfloat BALL_RADIUS = 12.5f;

class Game
{
public:
    // 游戏状态
    GameState  State;
    GLboolean  Keys[1024];          // 键盘按键
    GLuint     Width, Height;       // 屏幕宽度和高度
    std::vector<GameLevel> Levels;  // 关卡数组
    GLuint                 Level;   // 关卡
    std::vector<PowerUp>  PowerUps; // 补给
    GLuint Lives;                   // 生命
    GLboolean KeysProcessed[1024];  // 控制单次点击
    // 构造函数/析构函数
    Game(GLuint width, GLuint height);
    ~Game();
    // 初始化游戏状态（加载所有的着色器/纹理/关卡）
    void Init();
    // 游戏循环
    void ProcessInput(GLfloat dt);
    void Update(GLfloat dt);
    void Render();
    void RenderFrame(GLfloat dt);
    void DoCollisions(glm::vec2 lastPosition);
    // 重置
    void ResetLevel();
    void ResetPlayer();
    // 道具
    void SpawnPowerUps(GameObject& block);
    void UpdatePowerUps(GLfloat dt);
};

#endif