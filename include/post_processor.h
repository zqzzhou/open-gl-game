#pragma once
#ifndef POST_PROCESSOR_H
#define POST_PROCESSOR_H

#include "shader.h"
#include "texture.h"

class PostProcessor
{
public:
    // 状态
    Shader PostProcessingShader;
    Texture2D Texture;
    GLuint Width, Height;
    // 后处理选项（迷惑--纹理反色上下左右颠倒，混乱--边缘锐化画面随时间上下左右移动，晃动--短时间画面模糊上下左右轻微抖动）
    GLboolean Confuse, Chaos, Shake;
    // 构造函数
    PostProcessor(Shader shader, GLuint width, GLuint height);
    // 在渲染游戏之前准备后处理器的帧缓冲区操作
    void BeginRender();
    // 应该在渲染游戏后调用，以便将所有渲染数据存储到纹理对象中
    void EndRender();
    // 渲染后处理器纹理四边形（作为包含大型精灵的屏幕）
    void Render(GLfloat time);
private:
    // 渲染状态
    GLuint MSFBO, FBO; // MSFBO = 多重采样 FBO. FBO是常规的，用于将MS颜色缓冲区切换到纹理
    GLuint RBO; // RBO用于多重采样颜色缓冲区
    GLuint VAO;
    // 初始化用于渲染后处理纹理的四边形
    void initRenderData();
};

#endif