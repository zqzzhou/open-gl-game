#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <vector>

// 定义相机移动的几个可能选项。用作抽象，以远离窗口系统特定的输入方法
enum class Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// 设置默认摄像机值
const float YAW = -90.0f;           // 偏航角
const float PITCH = 0.0f;           // 俯仰角
const float SPEED = 2.5f;           // 摄影机移动速度
const float SENSITIVITY_X = 0.1f;   // 鼠标x轴灵敏度
const float SENSITIVITY_Y = 0.1f;   // 鼠标y轴灵敏度
const float ZOOM = 45.0f;           // 缩放


// 一个抽象摄像机类，用于处理输入并计算相应的欧拉角、向量和矩阵，以供OpenGL使用
class Camera
{
public:
    // 摄像机属性
    glm::vec3 Position;         // 位置
    glm::vec3 Front;            // 摄像机方向
    glm::vec3 Up;               // 上轴
    glm::vec3 Right;            // 右轴
    glm::vec3 WorldUp;          // 世界空间上向量
    // 欧拉角
    float Yaw;                  // 偏航角
    float Pitch;                // 俯仰角
    // 摄像机选项
    float MovementSpeed;        // 移动速度
    float MouseSensitivity_X;   // 鼠标x轴灵敏度
    float MouseSensitivity_Y;   // 鼠标y轴灵敏度
    float Zoom;                 // 缩放

    // 用向量构造摄像机对象
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity_X(SENSITIVITY_X), MouseSensitivity_Y(SENSITIVITY_Y), Zoom(ZOOM)
    {
        Position = position;
        WorldUp = up;
        Yaw = yaw;
        Pitch = pitch;
        updateCameraVectors();
    }
    // 用标量构造摄像机对象
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity_X(SENSITIVITY_X), MouseSensitivity_Y(SENSITIVITY_Y), Zoom(ZOOM)
    {
        Position = glm::vec3(posX, posY, posZ);
        WorldUp = glm::vec3(upX, upY, upZ);
        Yaw = yaw;
        Pitch = pitch;
        updateCameraVectors();
    }

    // 返回使用欧拉角和观察矩阵计算的视图矩阵
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(Position, Position + Front, Up);
    }

    // 它处理从任何类似键盘的输入系统接收的输入。接受摄像机定义的枚举类形式的输入参数（从窗口系统中提取）
    void ProcessKeyboard(Camera_Movement direction, float deltaTime)
    {
        float velocity = MovementSpeed * deltaTime;
        if (direction == Camera_Movement::FORWARD)
            Position += Front * velocity;
        if (direction == Camera_Movement::BACKWARD)
            Position -= Front * velocity;
        if (direction == Camera_Movement::LEFT)
            Position -= Right * velocity;
        if (direction == Camera_Movement::RIGHT)
            Position += Right * velocity;
        // 确保角色停留在地面（xz平面）
        //Position.y = 0.0f;
    }

    // 处理从鼠标输入系统接收的输入，需要x和y方向上的偏移值。
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
    {
        xoffset *= MouseSensitivity_X;
        yoffset *= MouseSensitivity_Y;

        Yaw += xoffset;
        Pitch += yoffset;

        // 确保当Pitch超出边界时，屏幕不会翻转
        if (constrainPitch)
        {
            if (Pitch > 89.0f)
                Pitch = 89.0f;
            if (Pitch < -89.0f)
                Pitch = -89.0f;
        }

        // 使用新的欧拉角更新 Front、Right 和 Up 向量
        updateCameraVectors();
    }

    // 用四元数处理从鼠标输入系统接收的输入，需要x和y方向上的偏移值。
    void ProcessMouseMovementByQuat(float xoffset, float yoffset, GLboolean constrainPitch = true)
    {
        xoffset *= MouseSensitivity_X;
        yoffset *= MouseSensitivity_Y;

        // 使用新的欧拉角更新 Front、Right 和 Up 向量
        updateCameraVectorsByQuat(xoffset, yoffset);
    }

    // 处理从鼠标滚轮事件接收的输入，仅需要垂直轮轴上的输入
    void ProcessMouseScroll(float yoffset)
    {
        Zoom -= (float)yoffset;
        if (Zoom < 1.0f)
            Zoom = 1.0f;
        if (Zoom > 45.0f)
            Zoom = 45.0f;
    }

private:
    // 根据摄像机的（更新的）欧拉角计算前向量
    void updateCameraVectors()
    {
        // 计算新的 Front 向量
        glm::vec3 front;
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        Front = glm::normalize(front);
        // 重新计算 Right 和 Up 向量
        Right = glm::normalize(glm::cross(Front, WorldUp));  // 标准化向量，因为它们的长度越向上或向下看越接近0，这会导致移动速度变慢。
        Up = glm::normalize(glm::cross(Right, Front));
    }

    // 从向量 start 旋转到 dest 的四元数
    glm::quat RotationBetweenVectors(glm::vec3 start, glm::vec3 dest) {
        start = normalize(start);
        dest = normalize(dest);

        float cosTheta = dot(start, dest);
        glm::vec3 rotationAxis;

        if (cosTheta < -1 + 0.001f) {
            // 向量方向相反时的特例：没有“理想”旋转轴
            // 所以要进行猜测，只要垂直于 start 都可以 
            rotationAxis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
            if (glm::length2(rotationAxis) < 0.01) // 当 start 与 glm::vec3(0.0f, 0.0f, 1.0f) 平行时，就与 glm::vec3(1.0f, 0.0f, 0.0f) 叉乘
                rotationAxis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);

            rotationAxis = normalize(rotationAxis);
            return glm::angleAxis(180.0f, rotationAxis);
        }

        rotationAxis = cross(start, dest);

        float s = sqrt((1 + cosTheta) * 2);
        float invs = 1 / s;

        return glm::quat(
            s * 0.5f,
            rotationAxis.x * invs,
            rotationAxis.y * invs,
            rotationAxis.z * invs
        );

    }

    // 根据摄像机的（更新的）四元数计算向量
    void updateCameraVectorsByQuat(float xoffset, float yoffset)
    {
        glm::vec3 ViewDest = Front + Right * xoffset * 0.008f + Up * yoffset * 0.008f;  // 目的向量
        if (glm::distance(ViewDest, WorldUp) > 0.1f && glm::distance(ViewDest, -WorldUp) > 0.1f) {
            // 当目的向量和世界坐标上下轴接近时不更新摄像机，防止天旋地转
            glm::quat rot1 = RotationBetweenVectors(Front, ViewDest);   // 获取旋转四元数
            // 用旋转四元数更新向量
            Front = glm::normalize(glm::rotate(rot1, Front));
            Right = glm::normalize(glm::cross(Front, WorldUp));
            Up = glm::normalize(glm::cross(Right, Front));
        }
    }
};
#endif