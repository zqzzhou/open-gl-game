#pragma once
#ifndef POWER_UP_H
#define POWER_UP_H

#include "game_object.h"

const glm::vec2 POWERUP_SIZE(60, 20);
const glm::vec2 POWERUP_VELOCITY(0.0f, 150.0f);

class PowerUp : public GameObject
{
public:
    // 道具类型
    std::string Type;
    GLfloat     Duration;
    GLboolean   Activated;
    // 构造函数
    PowerUp(std::string type, glm::vec3 color, GLfloat duration,
        glm::vec2 position, Texture2D texture)
        : GameObject(position, POWERUP_SIZE, texture, color, POWERUP_VELOCITY),
        Type(type), Duration(duration), Activated()
    { }
};

#endif