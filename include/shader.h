#pragma once
#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <iostream>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
    // 状态
    GLuint ID;
    // 构造函数
    Shader() { }
    // 激活当前的着色器
    Shader& Use();
    // 编译着色器代码
    void    Compile(const GLchar* vertexSource, const GLchar* fragmentSource, const GLchar* geometrySource = nullptr); // 几何着色器代码为可选项
    // Uniform设置函数
    void    SetFloat(const GLchar* name, GLfloat value, GLboolean useShader = false);
    void    SetFloatv(const GLchar* name, GLfloat* value, GLsizei size, GLboolean useShader = false);
    void    SetInt(const GLchar* name, GLint value, GLboolean useShader = false);
    void    SetIntv(const GLchar* name, GLint* value, GLsizei size, GLboolean useShader = false);
    void    SetVec2(const GLchar* name, GLfloat x, GLfloat y, GLboolean useShader = false);
    void    SetVec2(const GLchar* name, const glm::vec2& value, GLboolean useShader = false);
    void    SetVec2(const GLchar* name, const glm::vec2& value, GLsizei size,  GLboolean useShader = false);
    void    SetVec2(const GLchar* name, GLfloat* value, GLsizei size, GLboolean useShader = false);
    void    SetVec3(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader = false);
    void    SetVec3(const GLchar* name, const glm::vec3& value, GLboolean useShader = false);
    void    SetVec3(const GLchar* name, const glm::vec3& value, GLsizei size, GLboolean useShader = false);
    void    SetVec4(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader = false);
    void    SetVec4(const GLchar* name, const glm::vec4& value, GLboolean useShader = false);
    void    SetVec4(const GLchar* name, const glm::vec4& value, GLsizei size, GLboolean useShader = false);
    void    SetMat2(const GLchar* name, const glm::mat2& matrix, GLboolean useShader = false);
    void    SetMat3(const GLchar* name, const glm::mat3& matrix, GLboolean useShader = false);
    void    SetMat4(const GLchar* name, const glm::mat4& matrix, GLboolean useShader = false);
private:
    // 检查编译错误，并输出错误信息
    void    checkCompileErrors(GLuint object, std::string type);
};

#endif